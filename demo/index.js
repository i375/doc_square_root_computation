var readlineSync   = require('readline-sync');

var squareRootFrom = parseFloat(readlineSync.question("Enter square root from: "))
var sqrtAproxLow   = 0.0
var sqrtAproxHigh  = squareRootFrom
var sqrtAprox      = (sqrtAproxLow + sqrtAproxHigh) / 2.0
var precision      = parseFloat(readlineSync.question("Enter precision like 0.0001: "))
var deviation      = Math.pow(sqrtAprox, 2) - squareRootFrom

console.log(`Computing square root from ${squareRootFrom}`)

while (Math.abs(deviation) > precision) {

  if (Math.pow(sqrtAprox, 2) < squareRootFrom) {
    sqrtAproxLow = sqrtAprox
  } else if (Math.pow(sqrtAprox, 2) > squareRootFrom) {
    sqrtAproxHigh = sqrtAprox
  }
  sqrtAprox = (sqrtAproxLow + sqrtAproxHigh) / 2.0
  deviation = Math.pow(sqrtAprox, 2) - squareRootFrom

  console.log(
`
sqrtAproxLow  = ${sqrtAproxLow} 
sqrtAproxHigh = ${sqrtAproxHigh} 
sqrtAprox     = ${sqrtAprox} 
sqrtAprox^2   = ${Math.pow(sqrtAprox, 2)}`)

  readlineSync.question("press ENTER to contienue")
}


console.log(`\nSquare root is: ${sqrtAprox}\n`)
