# Visual guide to Square root computation #
_Ivane G. [9221145.com](http://9221145.com)_ 

## The source 
_Javascript, under NodeJS_  
_And there's dependency on NodeJS module `readline-sync`, which can be aqcuired with `npm` command `npm install readline-sync`_ 

```Javascript
var readlineSync   = require('readline-sync');

var squareRootFrom = parseFloat(readlineSync.question("Enter square root from: "))
var sqrtAproxLow   = 0.0
var sqrtAproxHigh  = squareRootFrom
var sqrtAprox      = (sqrtAproxLow + sqrtAproxHigh) / 2.0
var precision      = parseFloat(readlineSync.question("Enter precision like 0.0001: "))
var deviation      = Math.pow(sqrtAprox, 2) - squareRootFrom

console.log(`Computing square root from ${squareRootFrom}`)

while (Math.abs(deviation) > precision) {

  if (Math.pow(sqrtAprox, 2) < squareRootFrom) {
    sqrtAproxLow = sqrtAprox
  } else if (Math.pow(sqrtAprox, 2) > squareRootFrom) {
    sqrtAproxHigh = sqrtAprox
  }
  sqrtAprox = (sqrtAproxLow + sqrtAproxHigh) / 2.0
  deviation = Math.pow(sqrtAprox, 2) - squareRootFrom

  console.log(
`
sqrtAproxLow  = ${sqrtAproxLow} 
sqrtAproxHigh = ${sqrtAproxHigh} 
sqrtAprox     = ${sqrtAprox} 
sqrtAprox^2   = ${Math.pow(sqrtAprox, 2)}`)

  readlineSync.question("press ENTER to contienue")
}


console.log(`\nSquare root is: ${sqrtAprox}\n`)

```

## Textual and graphical interpretations of the process of computing/approximating square root

### Initialization

_I'll asume `squareRootFrom` equals `5` and `precision` equals `0.01`_ 

Initially get `squareRootFrom` from user (say it's `5`). That's the value we will compute square root for.  
```Javascript
var squareRootFrom = parseFloat(readlineSync.question("Enter square root from: "))
```

Set `sqrtAproxLow` to 0 and `sqrtAproxHigh` to value of `squareRootFrom`. These values define segment in middle of which is our `sqrtAprox` approximate value of square root from `squareRootFrom`.  
_Important point is that `sqrtAproxLow ^ 2` shell always be less than `squareRootFrom` and `sqrtAproxHigh ^ 2` shell always be kept more than `squareRootFrom`,  
meaning `(sqrtAproxLow ^ 2) > squareRootFrom < (sqrtAproxHigh ^ 2)`._  
```Javascript
var sqrtAproxLow   = 0.0  
var sqrtAproxHigh  = squareRootFrom
```

Set `sqrtAprox` to middle point between `sqrtAproxLow` and `sqrtAproxHigh`. 
```Javascript
var sqrtAprox      = (sqrtAproxLow + sqrtAproxHigh) / 2.0
```

Get `precision` from user input (_by the way, that's not mandatory_). That value defines segment near `sqrtFrom` in which `sqrtAprox ^ 2` shell fall, for computation to be stopped, meaning `sqrtAprox` is precise enough.  
Below is graphical representation of segment near `sqrtFrom` (in case `sqrtFrom` equalt to `5`)  
![precision segment](res/1.png) 

At this point, state graphically looks like this  
![step 0](res/2.png)  

### Main iteration loop (refining `sqrtAprox`)

Computation continues until absolute value of `deviation` gets less than `precision`, meaning `sqrtAprox ^ 2` falls inside segment between `squareRootFrom - precision` and `squareRootFrom + precision`.  
```Javascript
while (Math.abs(deviation) > precision) {
```

Next we check weather `sqrtAprox ^ 2` is less than `squareRootFrom`. _If condition succeeds_, as `sqrtAprox` is middle point between `sqrtAproxLow` and `sqrtAproxHigh`, we move `sqrtAprox` up a little bit up by setting `sqrtAproxLow` to `sqrtAprox`'s current value (preserving fact that `sqrtAproxLow ^ 2` is less than `squareRootFrom`).  
_**Note:** We've shrinked segment `[sqrtAproxLow, sqrtAproxHigh]` in half._  
_**Note:** It's important to check `Math.pow(sqrtAprox, 2) < squareRootFrom` and move `sqrtAproxLow` up, before checking `Math.pow(sqrtAprox, 2) > squareRootFrom` and moving `sqrtAproxHigh` down, because otherwise we'd loose chance to push `sqrtAprox` up, near to `squareRootFrom`._
```Javascript
  if (Math.pow(sqrtAprox, 2) < squareRootFrom) {
    sqrtAproxLow = sqrtAprox
```

If above condition fails then we check weather `sqrtAprox ^ 2` is more than `squareRootFrom` and move `sqrtAprox` down a bit, by setting `sqrtAproxHigh` to `sqrtAprox`'s current value.  
_**Note:** still preserving fact that `sqrtAproxHigh ^ 2` is more than `squareRootFrom`_  
_**Note:** We've shrinked segment `[sqrtAproxLow, sqrtAproxHigh]` in half, again._  
```Javascript
  } else if (Math.pow(sqrtAprox, 2) > squareRootFrom) {
    sqrtAproxHigh = sqrtAprox
  }
```

Next we update `sqrtAprox` by setting it to current middle point between `sqrtAproxLow` and `sqrtAproxHigh`, and update `deviation` to decide weather we shell continue or not the tomputation.  
```Javascript
  sqrtAprox = (sqrtAproxLow + sqrtAproxHigh) / 2.0
  deviation = Math.pow(sqrtAprox, 2) - squareRootFrom
```

Through this process of shrinking segment `[sqrtAproxLow, sqrtAproxHigh]` and moving `sqrtAprox ^ 2` nearer and nearer to `squareRootFrom` we get to the point when distance between `sqrtAprox ^ 2` and `squareRootFrom` get less than `precision`, and that's where we top computation and use `sqrtAprox`'s value as a result of computation.

Bellow are few states at various steps in graphical form.

State at `Step 1` (after initialization, here we need to push `sqrtAprox` up)  
![step 1](res/3.png)

State at `Step 2`(here we need to push `sqrtAprox` up again)  
![step 2](res/4.png)

State at `Step 3` (here we need to push `sqrtAprox` up again)  
![step 3](res/5.png)

And so on, pushing up, pushing down, getting closer to `sqrtFrom`, decreasing `deviation` step by step.

That's it :) . 

_Note that that's not the only way of computing square root. In school you might been presented with diferent formula. You can use more simple but costy algorithm and so on. But here this is binary searching square root from some value, with customizable `precision`._





